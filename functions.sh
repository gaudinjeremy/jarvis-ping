#!/bin/bash

#MODULE PING: pour intéroger une adresse IP enregistrer dans la mémoire de Jarvis

js_ping_checkIp(){

    ip=$(js_ia_getStrictText $1 $2)
    ping -c 1 $ip 1>/dev/null 2>/dev/null
    if [ $? -eq 0 ]
        then
            echo "true"
    else
        echo "false"
    fi
}
